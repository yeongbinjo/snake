#include <ncurses.h>
#include <panel.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <stdlib.h>

#define QUEUE_MAX_SIZE 1024

#define MAP_ROW         21
#define MAP_COL         41

typedef struct {
        int queue[QUEUE_MAX_SIZE];
        int front;
        int rear;
        int count;
}Queue;

typedef struct _Body {
        int y;
        int x;
        struct _Body *next;
}Body;

typedef struct {
        Body *head;
        Body *tail;
        int direction;
        int size;
}Snake;

void initSnake(Snake *s);
void setBody(Body *b, int y, int x, Body *next);
void eatApple(Snake *s, int apple_y, int apple_x);
void moveSnake(Snake *s, int move_y, int move_x);
int Crash_or_Eat(int next_y, int next_x, int map[MAP_ROW][MAP_COL]);

void initQueue(Queue *q);
int isFull(Queue *q);
int isEmpty(Queue *q);
void enqueue(Queue *q, int input);
int dequeue(Queue *q);

/* intro animation */
void intro(void);

int game(int stage);
void printMap(WINDOW *map_win, int map[MAP_ROW][MAP_COL]);
void printScore(WINDOW *score_win, Snake *s, int current_stage, int total_score);
void printClear(int clear_y, int clear_x);
void printPause(int pause_y, int pause_x);
int printAllClear(int allclear_y, int allclear_x, int score);
void submitScore(WINDOW *allclear_win, int score);
int gameover(int over_y, int over_x);

WINDOW* create_newwin(int height, int width, int y, int x, int color);
