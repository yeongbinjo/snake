.SUFFIXES : .c .o

CC = gcc

OBJS = main.o animation.o
SRCS = main.c animation.c
CFLAGS = -lncurses -lpanel

TARGET = snake

$(TARGET) : $(OBJS)
	$(CC) -o $(TARGET) $(OBJS) $(CFLAGS)

main.o : snake.h main.c
	$(CC) -c main.c

animation.o : snake.h animation.c
	$(CC) -c animation.c

clean :
	rm -rf $(OBJS) $(TARGET)

new :
	$(MAKE) clean
	$(MAKE)
