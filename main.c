/*
 * 리눅스 콘솔용 게임 스네이크
 *
 * 개발자	: eM (whoknowwhat0623@gmail.com)
 * 날짜		: 2010-05
 * 개발환경	: Fedora 9
 * 개발언어	: C 
 * 컴파일러	: GNU C Compiler 4.3.0 20080428
 */

#include "snake.h"

#define RIGHT   1
#define LEFT    2
#define UP      3
#define DOWN    4

#define TOTAL_STAGE     5
#define MAX_SNAKE_SIZE  24

#define BODY_SHAPE      '0'
#define HEAD_SHAPE      '0'
#define APPLE_SHAPE     '@'

#define SCORE_ROW       21
#define SCORE_COL       15
#define CLEAR_ROW       5
#define CLEAR_COL       19
#define PAUSE_ROW       3
#define PAUSE_COL       15
#define ALLCLEAR_ROW	13
#define ALLCLEAR_COL	37
#define OVER_ROW        7
#define OVER_COL        27

int CLOCK_FILTER = 20000;

int main(int argc, char* argv[]) {
        if( argc > 2 ) {
                printf("usage : %s speed[10000~1000000] (default : 20000)\n", argv[0]);
                exit(EXIT_FAILURE);
        }
        else if ( argc == 2 ) {
                CLOCK_FILTER = atoi(argv[1]);
                if( CLOCK_FILTER < 10000 || CLOCK_FILTER > 1000000 ) {
                        printf("usage : %s speed[10000~1000000] (default : 20000)\n", argv[0]);
                        exit(EXIT_FAILURE);
                }
        }
        initscr();
        start_color();
        curs_set(0);
        noecho();
        cbreak();
        keypad(stdscr, TRUE);

        init_pair(1, COLOR_RED, COLOR_BLACK);
        init_pair(2, COLOR_GREEN, COLOR_BLACK);
        init_pair(3, COLOR_YELLOW, COLOR_BLACK);
        init_pair(4, COLOR_BLUE, COLOR_BLACK);
        init_pair(5, COLOR_MAGENTA, COLOR_BLACK);
        init_pair(6, COLOR_CYAN, COLOR_BLACK);
        init_pair(7, COLOR_WHITE, COLOR_BLACK);
        init_pair(8, COLOR_WHITE, COLOR_YELLOW); // map frame
        init_pair(9, COLOR_WHITE, COLOR_BLUE); // quit frame
        init_pair(10, COLOR_BLACK, COLOR_WHITE); // map frame

        intro();

        int stage = 1;

        while( stage = game(stage) );

        endwin();

        return 0;
}

int game(int stage) {
        int bg_map[TOTAL_STAGE][MAP_ROW][MAP_COL] = {
                1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
                1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,
                1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,
                1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,
                1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,
                1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,
                1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,
                1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,
                1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,
                1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,
                1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,
                1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,
                1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,
                1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,
                1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,
                1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,
                1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,
                1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,
                1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,
                1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,
                1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
                1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
                1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,
                1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,
                1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,
                1,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,1,
                1,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,1,
                1,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,1,
                1,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,1,
                1,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,1,
                1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,
                1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,
                1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,
                1,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,1,
                1,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,1,
                1,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,1,
                1,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,1,
                1,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,1,
                1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,
                1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,
                1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,
                1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
                1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
                1,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,1,
                1,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,1,
                1,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,1,
                1,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,1,
                1,0,0,0,0,0,0,1,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,1,0,0,0,0,0,0,1,
                1,0,0,0,0,0,0,1,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,1,0,0,0,0,0,0,1,
                1,0,0,0,0,0,0,1,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,1,0,0,0,0,0,0,1,
                1,0,0,0,0,0,0,1,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,1,0,0,0,0,0,0,1,
                1,0,0,0,0,0,0,1,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,1,0,0,0,0,0,0,1,
                1,0,0,0,0,0,0,1,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,1,0,0,0,0,0,0,1,
                1,0,0,0,0,0,0,1,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,1,0,0,0,0,0,0,1,
                1,0,0,0,0,0,0,1,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,1,0,0,0,0,0,0,1,
                1,0,0,0,0,0,0,1,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,1,0,0,0,0,0,0,1,
                1,0,0,0,0,0,0,1,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,1,0,0,0,0,0,0,1,
                1,0,0,0,0,0,0,1,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,1,0,0,0,0,0,0,1,
                1,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,1,
                1,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,1,
                1,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,1,
                1,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,1,
                1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
                1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
                1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,
                1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,
                1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,
                1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,
                1,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,1,
                1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,
                1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,
                1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,
                1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,
                1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,
                1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,
                1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,
                1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,
                1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,
                1,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,1,
                1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,
                1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,
                1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,
                1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,
                1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
                1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
                1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,
                1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,
                1,0,0,1,0,0,1,0,0,1,0,0,1,0,0,1,0,0,1,0,0,0,1,0,0,1,0,0,1,0,0,1,0,0,1,0,0,1,0,0,1,
                1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,
                1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,
                1,0,0,1,0,0,1,0,0,1,0,0,1,0,0,1,0,0,1,0,0,0,1,0,0,1,0,0,1,0,0,1,0,0,1,0,0,1,0,0,1,
                1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,
                1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,
                1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,
                1,0,0,1,0,0,1,0,0,1,0,0,1,0,0,1,0,0,0,0,0,0,0,0,0,1,0,0,1,0,0,1,0,0,1,0,0,1,0,0,1,
                1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,
                1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,
                1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,
                1,0,0,1,0,0,1,0,0,1,0,0,1,0,0,1,0,0,1,0,0,0,1,0,0,1,0,0,1,0,0,1,0,0,1,0,0,1,0,0,1,
                1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,
                1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,
                1,0,0,1,0,0,1,0,0,1,0,0,1,0,0,1,0,0,1,0,0,0,1,0,0,1,0,0,1,0,0,1,0,0,1,0,0,1,0,0,1,
                1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,
                1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,
                1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
        };

        WINDOW *map_win, *score_win; // 윈도우
        PANEL *map_panel, *score_panel; // 패널
        int nmap[MAP_ROW][MAP_COL]; // 출력용 맵
        int map_y, map_x, score_y, score_x, clear_y, clear_x, pause_y, pause_x, allclear_y, allclear_x, over_y, over_x; // 윈도우 좌표들
        int current_stage = stage - 1;
        int total_score = 0;
        int quit_or_restart_flag = -1;

        /* 윈도우 좌표 초기화 */
        map_y = (LINES - MAP_ROW) / 2 + 1;
        map_x = (COLS - (MAP_COL + SCORE_COL + 4 )) / 2;
        score_y = (LINES - SCORE_ROW) / 2 + 1;
        score_x = map_x + MAP_COL + 4;
        clear_y = map_y + MAP_ROW / 5;
        clear_x = map_x + (MAP_COL - CLEAR_COL) / 2;
        pause_y = map_y + MAP_ROW / 5;
        pause_x = map_x + (MAP_COL - PAUSE_COL) / 2;
        allclear_y = map_y + (MAP_ROW - ALLCLEAR_ROW) / 2;
        allclear_x = map_x + (MAP_COL - ALLCLEAR_COL) / 2;
        over_y = map_y + MAP_ROW / 5;
        over_x = map_x + (MAP_COL - OVER_COL) / 2;


        /* 윈도우 생성 및 패널 등록 */
        map_win = create_newwin(MAP_ROW, MAP_COL, map_y, map_x, 8);
        score_win = create_newwin(SCORE_ROW, SCORE_COL, score_y, score_x, 8);
        map_panel = new_panel(map_win);
        score_panel = new_panel(score_win);

        update_panels();
        doupdate();

        /* 명령 큐를 생성하고 초기화 */
        Queue command_q;

        initQueue(&command_q);

        /* 뱀 생성 및 초기화 */
        Snake snake;

        initSnake(&snake);

        /* 뱀 첫 출력 */
        int i, j;
        for( i=0 ; i<MAP_ROW ; i++ )
                for( j=0 ; j<MAP_COL ; j++ )
                        nmap[i][j] = bg_map[0][i][j];

        Body *temp = snake.tail;
        for( i=0 ; i<snake.size-1 ; i++ ) {
                nmap[temp->y][temp->x] = 2;
                temp = temp->next;
        }
        nmap[temp->y][temp->x] = 3;

        printMap(map_win, nmap);

        /* 점수 첫 출력 */
        printScore(score_win, &snake, current_stage, total_score);

        int ch;

        int move_delay = 1500;
        int move_time = clock()/(CLOCKS_PER_SEC/CLOCK_FILTER) + move_delay;

        srand((int)time(NULL));
        int apple_delay = rand()%CLOCK_FILTER + CLOCK_FILTER;
        int apple_time = clock()/(CLOCKS_PER_SEC/CLOCK_FILTER) + apple_delay;
        int apple_y, apple_x;

        int next_y, next_x;
        int temp_tail_y, temp_tail_x;

        nodelay(stdscr, TRUE);

        /* 게임 시작 */
        while(1) {
                /* 사과 생성 */
                if( apple_time < clock()/(CLOCKS_PER_SEC/CLOCK_FILTER) ) {
                        next_y = rand() % (MAP_ROW - 2) + 1;
                        next_x = rand() % (MAP_COL - 2) + 1;
                        while( nmap[next_y][next_x] ) {
                                next_y = rand() % (MAP_ROW - 2) + 1;
                                next_x = rand() % (MAP_COL - 2) + 1;
                        }
                        bg_map[current_stage][next_y][next_x] = 4;

                        apple_delay = rand()%(CLOCK_FILTER/3) + CLOCK_FILTER;
                        apple_time = clock()/(CLOCKS_PER_SEC/CLOCK_FILTER) + apple_delay;
                }

                /* 다음 이동 좌표를 계산해서 이동, 증식, 충돌, 넥스트 스테이지를 결정 */
                if( move_time < clock()/(CLOCKS_PER_SEC/CLOCK_FILTER) ) {
                        if( snake.direction == UP ) {
                                next_y = snake.head->y - 1;
                                next_x = snake.head->x;
                        }
                        else if( snake.direction == DOWN ) {
                                next_y = snake.head->y + 1;
                                next_x = snake.head->x;
                        }
                        else if( snake.direction == RIGHT ) {
                                next_y = snake.head->y;
                                next_x = snake.head->x + 1;
                        }
                        else if( snake.direction == LEFT ) {
                                next_y = snake.head->y;
                                next_x = snake.head->x - 1;
                        }
                        temp_tail_y = snake.tail->y;
                        temp_tail_x = snake.tail->x;

                        /* 이동 */
                        if( !Crash_or_Eat(next_y, next_x, bg_map[current_stage]) )
                                moveSnake(&snake, next_y, next_x);
                        else if( Crash_or_Eat(next_y, next_x, bg_map[current_stage]) == 2 ) {
                                eatApple(&snake, next_y, next_x);
                                bg_map[current_stage][next_y][next_x] = 0;
                                printScore(score_win, &snake, current_stage, total_score);
                                if( snake.size == MAX_SNAKE_SIZE ) {
                                        for( i=0 ; i<MAP_ROW ; i++ )
                                                for( j=0 ; j<MAP_COL ; j++ )
                                                        nmap[i][j] = bg_map[current_stage][i][j];

                                        temp = snake.tail;
                                        for( i=0 ; i<snake.size-1 ; i++ ) {
                                                nmap[temp->y][temp->x] = 2;
                                                temp = temp->next;
                                        }
                                        nmap[temp->y][temp->x] = 3;

                                        printMap(map_win, nmap);

                                        if( current_stage == TOTAL_STAGE - 1 ) {
                                                quit_or_restart_flag = printAllClear(allclear_y, allclear_x, total_score);
                                                current_stage = 0;
                                                break;
                                        }
                                        else
                                                printClear(clear_y, clear_x);

                                        total_score += (snake.size - 4) * 50;
                                        current_stage += 1;

                                        initSnake(&snake);

                                        for( i=0 ; i<MAP_ROW ; i++ )
                                                for( j=0 ; j<MAP_COL ; j++ )
                                                        nmap[i][j] = bg_map[0][i][j];

                                        temp = snake.tail;
                                        for( i=0 ; i<snake.size-1 ; i++ ) {
                                                nmap[temp->y][temp->x] = 2;
                                                temp = temp->next;
                                        }
                                        nmap[temp->y][temp->x] = 3;

                                        printMap(map_win, nmap);

                                        printScore(score_win, &snake, current_stage, total_score);
                                }
                        }
                        if( Crash_or_Eat(next_y, next_x, nmap) == 1 )
                                if( !(next_y == temp_tail_y && next_x == temp_tail_x) ) {
                                        quit_or_restart_flag = gameover(over_y, over_x);
                                        break;
                                }

                        for( i=0 ; i<MAP_ROW ; i++ )
                                for( j=0 ; j<MAP_COL ; j++ )
                                        nmap[i][j] = bg_map[current_stage][i][j];

                        temp = snake.tail;
                        for( i=0 ; i<snake.size-1 ; i++ ) {
                                nmap[temp->y][temp->x] = 2;
                                temp = temp->next;
                        }
                        nmap[temp->y][temp->x] = 3;

                        printMap(map_win, nmap);

                        move_time = clock()/(CLOCKS_PER_SEC/CLOCK_FILTER) + move_delay;
                }

                if( ch = getch() )
                        if( ch == KEY_UP || ch == KEY_DOWN || ch == KEY_RIGHT || ch == KEY_LEFT || ch == 'p' )
                                enqueue(&command_q, ch);

                if( !isEmpty(&command_q) ) {
                        ch = dequeue(&command_q);
                        switch(ch) {
                                case KEY_UP:
                                        if( snake.direction != DOWN )
                                                snake.direction = UP;
                                        break;
                                case KEY_DOWN:
                                        if( snake.direction != UP )
                                                snake.direction = DOWN;
                                        break;
                                case KEY_RIGHT:
                                        if( snake.direction != LEFT )
                                                snake.direction = RIGHT;
                                        break;
                                case KEY_LEFT:
                                        if( snake.direction != RIGHT )
                                                snake.direction = LEFT;
                                        break;
                                case 'p':
                                        printPause(pause_y, pause_x);
                                        apple_time = clock()/(CLOCKS_PER_SEC/CLOCK_FILTER) + apple_delay;
                                        break;
                        }

                        if( snake.direction == UP ) {
                                next_y = snake.head->y - 1;
                                next_x = snake.head->x;
                        }
                        else if( snake.direction == DOWN ) {
                                next_y = snake.head->y + 1;
                                next_x = snake.head->x;
                        }
                        else if( snake.direction == RIGHT ) {
                                next_y = snake.head->y;
                                next_x = snake.head->x + 1;
                        }
                        else if( snake.direction == LEFT ) {
                                next_y = snake.head->y;
                                next_x = snake.head->x - 1;
                        }

                        temp_tail_y = snake.tail->y;
                        temp_tail_x = snake.tail->x;

                        /* 이동 */
                        if( !Crash_or_Eat(next_y, next_x, bg_map[current_stage]) )
                                moveSnake(&snake, next_y, next_x);
                        else if( Crash_or_Eat(next_y, next_x, bg_map[current_stage]) == 2 ) {
                                eatApple(&snake, next_y, next_x);
                                bg_map[current_stage][next_y][next_x] = 0;
                                printScore(score_win, &snake, current_stage, total_score);
                                if( snake.size == MAX_SNAKE_SIZE ) {
                                        for( i=0 ; i<MAP_ROW ; i++ )
                                                for( j=0 ; j<MAP_COL ; j++ )
                                                        nmap[i][j] = bg_map[current_stage][i][j];

                                        temp = snake.tail;
                                        for( i=0 ; i<snake.size-1 ; i++ ) {
                                                nmap[temp->y][temp->x] = 2;
                                                temp = temp->next;
                                        }
                                        nmap[temp->y][temp->x] = 3;

                                        printMap(map_win, nmap);

                                        if( current_stage == TOTAL_STAGE - 1 ) {
                                                quit_or_restart_flag = printAllClear(allclear_y, allclear_x, total_score);
                                                break;
                                        }
                                        else
                                                printClear(clear_y, clear_x);

                                        total_score += (snake.size - 4) * 50;
                                        current_stage += 1;

                                        initSnake(&snake);

                                        for( i=0 ; i<MAP_ROW ; i++ )
                                                for( j=0 ; j<MAP_COL ; j++ )
                                                        nmap[i][j] = bg_map[0][i][j];

                                        temp = snake.tail;
                                        for( i=0 ; i<snake.size-1 ; i++ ) {
                                                nmap[temp->y][temp->x] = 2;
                                                temp = temp->next;
                                        }
                                        nmap[temp->y][temp->x] = 3;

                                        printMap(map_win, nmap);

                                        printScore(score_win, &snake, current_stage, total_score);
                                }
                        }
                        if( Crash_or_Eat(next_y, next_x, nmap) == 1 )
                                if( !(next_y == temp_tail_y && next_x == temp_tail_x) ) {
                                        quit_or_restart_flag = gameover(over_y, over_x);
                                        break;
                                }

                        for( i=0 ; i<MAP_ROW ; i++ )
                                for( j=0 ; j<MAP_COL ; j++ )
                                        nmap[i][j] = bg_map[current_stage][i][j];

                        temp = snake.tail;
                        for( i=0 ; i<snake.size-1 ; i++ ) {
                                nmap[temp->y][temp->x] = 2;
                                temp = temp->next;
                        }
                        nmap[temp->y][temp->x] = 3;

                        printMap(map_win, nmap);

                        move_time = clock()/(CLOCKS_PER_SEC/CLOCK_FILTER) + move_delay;
                }
        }

        nodelay(stdscr, FALSE);

        return quit_or_restart_flag * (current_stage + 1);
}

void printMap(WINDOW *map_win, int map[MAP_ROW][MAP_COL]) {
        int y, x;
        for( y=1 ; y<MAP_ROW-1 ; y++ ) {
                for( x=1 ; x<MAP_COL-1 ; x++ ) {
                        if( map[y][x] == 1 ) {
                                wattron(map_win, COLOR_PAIR(8));
                                mvwaddch(map_win, y, x, ' ');
                                wattroff(map_win, COLOR_PAIR(8));
                        }
                        else if( map[y][x] == 2 ) {
                                wattron(map_win, COLOR_PAIR(2));
                                mvwaddch(map_win, y, x, BODY_SHAPE);
                                wattroff(map_win, COLOR_PAIR(2));
                        }
                        else if( map[y][x] == 3 ) {
                                wattron(map_win, COLOR_PAIR(1));
                                mvwaddch(map_win, y, x, HEAD_SHAPE);
                                wattroff(map_win, COLOR_PAIR(1));
                        }
                        else if( map[y][x] == 4 ) {
                                wattron(map_win, COLOR_PAIR(1));
                                mvwaddch(map_win, y, x, APPLE_SHAPE);
                                wattroff(map_win, COLOR_PAIR(1));
                        }
                        else
                                mvwaddch(map_win, y, x, ' ');
                }
        }

        wrefresh(map_win);

        return;
}

void printScore(WINDOW *score_win, Snake *s, int current_stage, int total_score) {
        int y, x;
        y = SCORE_ROW / 3 - 2;
        x = (SCORE_COL - strlen("S T A G E")) / 2;

        wattron(score_win, COLOR_PAIR(5));
        mvwprintw(score_win, y, x, "%s", "S T A G E");
        wattroff(score_win, COLOR_PAIR(5));

        y = SCORE_ROW / 3;
        x = SCORE_COL / 2;

        wattron(score_win, COLOR_PAIR(6));
        mvwprintw(score_win, y, x, "%d", current_stage + 1);
        wattroff(score_win, COLOR_PAIR(6));

        y = SCORE_ROW / 3 * 2 - 2;
        x = (SCORE_COL - strlen("S C O R E")) / 2;

        wattron(score_win, COLOR_PAIR(5));
        mvwprintw(score_win, y, x, "%s", "S C O R E");
        wattroff(score_win, COLOR_PAIR(5));

        y = SCORE_ROW / 3 * 2;
        x = (SCORE_COL - 5) / 2;

        wattron(score_win, COLOR_PAIR(6));
        mvwprintw(score_win, y, x, "%05d", total_score + (s->size - 4) * 50);
        wattroff(score_win, COLOR_PAIR(6));
        wrefresh(score_win);

        return;
}

void printClear(int clear_y, int clear_x) {
        int ch;

        WINDOW *clear_win;
        PANEL *clear_panel;

        clear_win = create_newwin(CLEAR_ROW, CLEAR_COL, clear_y, clear_x, 9);
        clear_panel = new_panel(clear_win);

        update_panels();
        doupdate();

        wattron(clear_win, COLOR_PAIR(5));
        mvwprintw(clear_win, CLEAR_ROW / 2, (CLEAR_COL - strlen("Stage  Clear!")) / 2, "Stage  Clear!");
        wattroff(clear_win, COLOR_PAIR(5));
        wrefresh(clear_win);

        nodelay(stdscr, FALSE);
        while( ch = getch() )
                if( ch == 10 )
                        break;
        nodelay(stdscr, TRUE);

        del_panel(clear_panel);

        update_panels();
        doupdate();
}

void printPause(int pause_y, int pause_x) {
        int ch;

        WINDOW *pause_win;
        PANEL *pause_panel;

        pause_win = create_newwin(PAUSE_ROW, PAUSE_COL, pause_y, pause_x, 9);
        pause_panel = new_panel(pause_win);

        update_panels();
        doupdate();

        wattron(pause_win, COLOR_PAIR(5));
        mvwprintw(pause_win, PAUSE_ROW / 2, (PAUSE_COL - strlen("P a u s e")) / 2, "P a u s e");
        wattroff(pause_win, COLOR_PAIR(5));
        wrefresh(pause_win);

        nodelay(stdscr, FALSE);
        while( ch = getch() )
                if( ch == 'p' )
                        break;
        nodelay(stdscr, TRUE);

        del_panel(pause_panel);

        update_panels();
        doupdate();
}

int printAllClear(int allclear_y, int allclear_x, int score) {
        int ch;

        WINDOW *allclear_win;
        PANEL *allclear_panel;

        allclear_win = create_newwin(ALLCLEAR_ROW, ALLCLEAR_COL, allclear_y, allclear_x, 9);
        allclear_panel = new_panel(allclear_win);

        update_panels();
        doupdate();

        wattron(allclear_win, COLOR_PAIR(5));
        mvwprintw(allclear_win, 2, (ALLCLEAR_COL - strlen("Congratulation!")) / 2, "Congratulation!");
        mvwprintw(allclear_win, 3, (ALLCLEAR_COL - strlen("All Stage Cleared")) / 2, "All Stage Cleared");

        mvwprintw(allclear_win, 5, (ALLCLEAR_COL - strlen("If you have a perfect game,")) / 2, "If you have a perfect game,");
        mvwprintw(allclear_win, 6, (ALLCLEAR_COL - strlen("you can get what you want.")) / 2, "you can get what you want.");

        wrefresh(allclear_win);

        submitScore(allclear_win, score);

        wattroff(allclear_win, COLOR_PAIR(5));
        del_panel(allclear_panel);

        update_panels();
        doupdate();

        return 0;
}

void submitScore(WINDOW *allclear_win, int score) {
        int submit_score = score;
        char name[10] = {0, };
        nodelay(stdscr, FALSE);
        mvwprintw(allclear_win, 9, 3, "Enter your name :: ");
        wrefresh(allclear_win);
        echo();
        nocbreak();
        wscanw(allclear_win, "%s", name);
        cbreak();
        noecho();

        mvwprintw(allclear_win, 10, 3, "Thank you for playing, %s", name);
        wrefresh(allclear_win);
        getch();
}

int gameover(int over_y, int over_x) {
        int ch;

        WINDOW *over_win;
        PANEL *over_panel;

        over_win = create_newwin(OVER_ROW, OVER_COL, over_y, over_x, 9);
        over_panel = new_panel(over_win);

        update_panels();
        doupdate();

        wattron(over_win, COLOR_PAIR(5));
        mvwprintw(over_win, OVER_ROW / 2 - 1, (OVER_COL - strlen("G A M E  O V E R")) / 2, "G A M E  O V E R");
        mvwprintw(over_win, OVER_ROW / 2 + 1, (OVER_COL - strlen("Quit or Restart? [q/r]")) / 2, "Quit or Restart? [q/r]");
        wattroff(over_win, COLOR_PAIR(5));
        wrefresh(over_win);

        nodelay(stdscr, FALSE);
        while( ch = getch() )
                if( ch == 'q' || ch == 'r' )
                        break;
        nodelay(stdscr, TRUE);

        del_panel(over_panel);

        update_panels();
        doupdate();

        if( ch == 'q' )
                return 0;
        else if( ch == 'r' )
                return 1;
}

WINDOW* create_newwin(int height, int width, int y, int x, int color) {
        int i, j;
        WINDOW *new_win;
        new_win = newwin(height, width, y, x);
        if( color >= 8 ) {
                wattron(new_win, COLOR_PAIR(10));
                wborder(new_win, ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ');
                wattroff(new_win, COLOR_PAIR(10));
                wattron(new_win, COLOR_PAIR(color));
                for( i=1 ; i<height ; i+=2 ) {
                        mvwaddch(new_win, i, 0, ' ');
                        mvwaddch(new_win, i, width-1, ' ');
                }
                for( i=1 ; i<width ; i+=2 ) {
                        mvwaddch(new_win, 0, i, ' ');
                        mvwaddch(new_win, height-1, i, ' ');
                }
                wattroff(new_win, COLOR_PAIR(color));
        }
        else {
                wattron(new_win, COLOR_PAIR(color));
                wborder(new_win, ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ');
                wattroff(new_win, COLOR_PAIR(color));
        }

        return new_win;
}

void initSnake(Snake *s) {
        Body *temp;
        int i;

        temp = (Body *)malloc(sizeof(Body) * 4);

        setBody(&temp[0], MAP_ROW / 2, (MAP_COL / 2) + 1, NULL);
        setBody(&temp[1], MAP_ROW / 2, (MAP_COL / 2) + 0, &temp[0]);
        setBody(&temp[2], MAP_ROW / 2, (MAP_COL / 2) - 1, &temp[1]);
        setBody(&temp[3], MAP_ROW / 2, (MAP_COL / 2) - 2, &temp[2]);

        s->head = &temp[0];
        s->tail = &temp[3];

        s->size = 4;
        s->direction = RIGHT;

        return;
}

void setBody(Body *b, int y, int x, Body *next) {
        b->y = y;
        b->x = x;
        b->next = next;

        return;
}

void eatApple(Snake *s, int apple_y, int apple_x) {
        Body *new_body;

        new_body = (Body *)malloc(sizeof(Body));

        setBody(new_body, apple_y, apple_x, NULL);
        s->head->next = new_body;
        s->head = new_body;
        s->size++;

        return;
}

void moveSnake(Snake *s, int move_y, int move_x) {
        Body *temp = s->tail;
        int i;
        for( i=0 ; i<(s->size - 1) ; i++ ) {
                temp->y = temp->next->y;
                temp->x = temp->next->x;
                temp = temp->next;
        }
        temp->y = move_y;
        temp->x = move_x;

        return;
}

int Crash_or_Eat(int next_y, int next_x, int map[MAP_ROW][MAP_COL]) {
        if( map[next_y][next_x] == 1 || map[next_y][next_x] == 2 )
                return 1; // crash
        else if( map[next_y][next_x] == 4 )
                return 2; // eat
        else
                return 0; // no event;
}

void initQueue(Queue *q) {
        q->front = -1;
        q->rear = 0;
        q->count = 0;

        return;
}

int isFull(Queue *q) {
        if( q->count == QUEUE_MAX_SIZE )
                return TRUE;
        else
                return FALSE;
}

int isEmpty(Queue *q) {
        if( q->count == 0 )
                return TRUE;
        else
                return FALSE;
}

void enqueue(Queue *q, int input) {
        if( isFull(q) )
                return;
        else {
                q->count++;
                q->queue[q->rear] = input;
                q->rear = (q->rear + 1) % QUEUE_MAX_SIZE;
                return;
        }
}

int dequeue(Queue *q) {
        if( isEmpty(q) )
                return 0;
        else {
                q->count--;
                q->front = (q->front + 1) % QUEUE_MAX_SIZE;
                return q->queue[q->front];
        }
}
